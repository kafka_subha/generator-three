package com.subha.kafka.processor;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by subha on 03/07/2018.
 */

@Component("genThree")
public class GeneratorThreeProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(GeneratorThreeProcessor.class);

    public void process(Exchange exchange) {
        int seed = Integer.parseInt(exchange.getIn().getBody().toString().split("-")[0]);
        int generatedResult = seed%10;

        LOG.info("Got seed " + seed);
        LOG.info("Generator one generated number " + generatedResult);
        exchange.getIn().setHeader("source", "GEN-THREE");
        exchange.getIn().setBody(generatedResult + "-GEN3-" + exchange.getIn().getBody().toString().split("-")[1]);
    }

}
